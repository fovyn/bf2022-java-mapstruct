package be.bf.formation.mapstruct;

import be.bf.formation.mapstruct.mappers.PersonMapper;
import be.bf.formation.mapstruct.models.dto.PersonDTO;
import be.bf.formation.mapstruct.models.entities.Address;
import be.bf.formation.mapstruct.models.entities.Person;
import be.bf.formation.mapstruct.models.entities.Sex;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.util.Hashtable;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        PersonMapper mapper = Mappers.getMapper(PersonMapper.class);

        Person p = new Person(1L, "Flavian", "Ovyn", LocalDate.of(1991, 7, 19), new Address(1L, "Chaussée de Nivelles", "89", "1472", "Trou Perdu"));
        p.sex = Sex.MALE;
        System.out.println(p);
        PersonDTO dto = mapper.fromEntities(p);
        dto.locality = p.getAddress().stream().map(it -> it.locality).collect(Collectors.toList());
        System.out.println(dto);
        Person p2 = mapper.toEntities(dto);
        System.out.println(p2);
    }
}
