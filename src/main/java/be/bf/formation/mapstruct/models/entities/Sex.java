package be.bf.formation.mapstruct.models.entities;

public enum Sex {
    MALE,
    FEMALE,
    OTHER
}
