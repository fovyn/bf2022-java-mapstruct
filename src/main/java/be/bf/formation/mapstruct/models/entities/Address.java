package be.bf.formation.mapstruct.models.entities;

import java.util.Objects;

public class Address {
    public Long id;
    public String street;
    public String number;
    public String postalCode;
    public String locality;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(street, address.street) && Objects.equals(number, address.number) && Objects.equals(postalCode, address.postalCode) && Objects.equals(locality, address.locality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, number, postalCode, locality);
    }

    public Address(Long id, String street, String number, String postalCode, String locality) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.postalCode = postalCode;
        this.locality = locality;
    }

    public Address() {
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", locality='" + locality + '\'' +
                '}';
    }
}
