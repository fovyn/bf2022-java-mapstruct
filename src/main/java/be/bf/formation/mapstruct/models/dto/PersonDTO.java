package be.bf.formation.mapstruct.models.dto;

import be.bf.formation.mapstruct.models.entities.Sex;
import com.google.common.base.MoreObjects;

import java.util.List;
import java.util.Objects;

public class PersonDTO {
    public String lastname;
    public String firstname;

    public List<String> locality;

    public Sex sex;


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("lastname", lastname)
                .add("firstname", firstname)
                .add("locality", locality)
                .add("sex", sex)
                .toString();
    }
}
