package be.bf.formation.mapstruct.models.entities;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Classe mutable représentant un membre de la société
 * FA = {id, lastname, firstname, birthDate}
 * @attribute id Long
 * @attribute lastname String
 * @attribute firstname String
 * @attribute birthDate LocalDate
 *
 * @invariant id != null > 0
 * @invariant lastname != null && lastname.length > 0
 */
public class Person {
    private Long id;
    private String lastname;
    private String firstname;
    private LocalDate birthDate;
    private List<Address> address = new ArrayList<>();

    public Sex sex;

    public Person() {
    }

    public Person(String lastname, String firstname, LocalDate birthDate) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthDate = birthDate;
    }

    public Person(Long id, String lastname, String firstname, LocalDate birthDate, Address address) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthDate = birthDate;
        this.address.add(address);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        if (lastname == null) throw new RuntimeException("Person.lastname must not be null");
        if (lastname.length() == 0) throw new RuntimeException("Person.lastname.length must be greater than 0");
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    /**
     *
     * @param lastname String | lastname != null AND lastname.length > 0
     * @throws RuntimeException e | lastname == null OR lastname.length <= 0
     * @return String
     * @modify this.lastname
     */
    public String sayHello(String lastname) {
        if (lastname == null || lastname.length() <= 0) {
            throw new RuntimeException("");
        }
        this.lastname = lastname;
        return "Hello "+ lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return lastname.equals(person.lastname) &&
                firstname.equals(person.firstname) &&
                birthDate.equals(person.birthDate) &&
                address.equals(person.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastname, firstname, birthDate, address);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("lastname", lastname)
                .add("firstname", firstname)
                .add("birthDate", birthDate)
                .add("address", address)
                .toString();
    }
}
