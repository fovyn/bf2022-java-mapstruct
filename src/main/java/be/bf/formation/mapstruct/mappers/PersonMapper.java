package be.bf.formation.mapstruct.mappers;

import be.bf.formation.mapstruct.models.dto.PersonDTO;
import be.bf.formation.mapstruct.models.entities.Person;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

@Mapper
public interface PersonMapper {
//    @ValueMapping(source = "sex", target = "sex")
    PersonDTO fromEntities(Person entities);
    Person toEntities(PersonDTO dto);
}
